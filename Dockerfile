FROM node:14

WORKDIR /home/app

RUN npm config set @revdebug:registry https://nexus.revdebug.com/repository/npm/

CMD  ["node", "app.js"]
