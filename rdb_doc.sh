# #!/bin/bash

REVDEBUG_JSON=./revdebug.json
PACKAGE_JSON=./package.json
REVDEBUG=./node_modules/@revdebug
REVDEBUG_REVDEBUG=./node_modules/@revdebug/revdebug/
REVDEBUG_SKYWALKING=./node_modules/@revdebug/skywalking/
DOCKERFILE=./Dockerfile

if [ -d "$REVDEBUG" ]; then
    echo "Reinstall"
    npx revd --remove --force

    rm -rf /node_modules package-lock.json

    npm install @revdebug/revdebug

    npm install

    npx revd
    echo "Reinstall done"
    bash rdb_chcek.sh
else
    echo "Install"
    if ! test -f "$PACKAGE_JSON"; then
        echo "npm init"
         npm init --yes
    fi
     npm install

     npm install @revdebug/revdebug

    if ! test -f "$REVDEBUG_JSON"; then
        echo "revdebug.json copy ......... CHANGE SETTINGS!"
        sudo cp ~/js_rdb_init/revdebug.json ./revdebug.json
    fi

     npx revd
    echo "Install done"
     bash rdb_chcek.sh
fi
