REVDEBUG_REVDEBUG=./node_modules/@revdebug/revdebug/
REVDEBUG_SKYWALKING=./node_modules/@revdebug/skywalking/
if [ -d "$REVDEBUG_REVDEBUG" ]; then
    echo "./node_modules/@revdebug/revdebug is exists"
else
    echo "./node_modules/@revdebug/revdebug MISSING!!!!!!!!!!!!!"
fi

if [ -d "$REVDEBUG_SKYWALKING" ]; then
    echo "./node_modules/@revdebug/skywalking is exists"
else
    echo "./node_modules/@revdebug/skywalking MISSING!!!!!!!!!!!!!"
fi


if [[ $EUID -ne 0 ]]; then
    sudo touch test_rdb.js
    sudo chmod 777 test_rdb.js
    var=./test_rdb.js
    sudo cat <<EOF >$var
var rdb= require('@revdebug/revdebug')
var sky= require('@revdebug/skywalking')
console.log(rdb)
console.log(sky)
EOF
    node test_rdb.js
    sudo rm test_rdb.js
else
    touch test_rdb.js
    chmod 777 test_rdb.js
    var=./test_rdb.js
    cat <<EOF >$var
var rdb= require('@revdebug/revdebug')
var sky= require('@revdebug/skywalking')
console.log(rdb)
console.log(sky)
EOF
    node test_rdb.js
    rm test_rdb.js

fi
