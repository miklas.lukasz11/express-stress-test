# Express stress test

Espress js endpoint application, for performance tests

## Installation

Run the script, rdb installation
```bash
./rdb_doc.sh
```

Set rdb host in revdebug.json
```json
{
    "host": "lmtestrdbserver.francecentral.cloudapp.azure.com",
    "mode": "live",
    "apm": true,
    "solution": "app_js",
    "application": "app",
    "files": "**/*.js"
}
```

Instrument the files by:
```bash
npx revd
```

Start application 
```bash 
node app.js
```

