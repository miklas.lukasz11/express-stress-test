var express = require('express');

// const {default: Agent, ContextManager} = require('@revdebug/skywalking');
// Agent.start({  // not necessary with REVDEBUG_APM=1
//   serviceName: 'app_express2',
//   // serviceInstance: '',
//    collectorAddress: '20.188.58.43:11800',
//   // authorization: '',
//   // maxBufferSize: 1000,
// });

const build_castle = require('./build_castle.js');
var app = express();

const PORT = 8000;
let simple_snapshot_call_count = 0;

app.get('/', function (req, res) {
  simple_snapshot_call_count++;
  res.send(String(simple_snapshot_call_count));
  revdebug.snapshot('Simple_snapshot',true);
});

app.get('/reset_call_counts', function (req, res) {
  simple_snapshot_call_count = 0;
  res.send('Resert all counts');
});

app.get('/err', function (req, res) {
  throw 'you shall not pass';
});

app.get('/build_castle', function (req, res) {
  let range = req.query.range;
  let poolOfExceptions = req.query.poolOfExceptions;
  if (range === undefined) {
    range=0
  }
  if (poolOfExceptions === undefined) {
    poolOfExceptions=6
  }

  let build = new build_castle(100, range, poolOfExceptions);
  res.send(build);
});

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}!`);
});
