var express = require('express');

var app = express();

const PORT = 8000;
let simple_snapshot_call_count = 0;

app.get('/', function (req, res) {
  simple_snapshot_call_count++;
  res.send(String(simple_snapshot_call_count));
  for (var x = 0; x < 10; x++) asd();
  asd()
  asd2()
  asd3()
  asd4()
  asd6()
  asd5()
  asd7()
  asd13()
  asd12()
  asd11()
  asd10()
  asd8()
  asd9()
  asd()
  asd10()
  asd11()
  asd()
  revdebug.snapshot('Simple_snapshot', true);
});

function asd() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}


function asd2() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd3() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd4() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}


function asd6() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd5() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd7() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}




function asd13() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd12() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd11() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}


function asd10() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd9() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}

function asd8() {
  for (var i = 0; i < 10; i++) {
    console.log(i);
    let fruits = [
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
      'Apple',
      'Banana',
    ];
    let first = fruits[0];

    let last = fruits[fruits.length - 1];

    fruits.forEach(function (item, index, array) {
      console.log(item, index);
    });
    let newLength = fruits.push('Orange');
    last = fruits.pop();
    first = fruits.shift();
    first = fruits.shift();
    newLength = fruits.push('Orange');
    fruits.push('Mango');
    let pos = fruits.indexOf('Banana');
    let vegetables = ['Cabbage', 'Turnip', 'Radish', 'Carrot'];
    console.log(vegetables);
    // ["Cabbage", "Turnip", "Radish", "Carrot"]

    pos = 1;
    let n = 2;

    let removedItems = vegetables.splice(pos, n);
    // this is how to remove items, n defines the number of items to be removed,
    // starting at the index position specified by pos and progressing toward the end of array.

    console.log(vegetables);
    // ["Cabbage", "Carrot"] (the original array is changed)

    console.log(removedItems);
    // ["Turnip", "Radish"]

    let shallowCopy = fruits.slice();

    let arr = [
      'this is the first element',
      'this is the second element',
      'this is the last element',
    ];
    console.log(arr[0]); // logs 'this is the first element'
    console.log(arr[1]); // logs 'this is the second element'
    console.log(arr[arr.length - 1]); // logs 'this is the last element'

    let today = new Date();
    let birthday = new Date('December 17, 1995 03:24:00');
     birthday = new Date('1995-12-17T03:24:00');
     birthday = new Date(1995, 11, 17); // the month is 0-indexed
     birthday = new Date(1995, 11, 17, 3, 24, 0);
     birthday = new Date(628021800000); // passing epoch timestamp

    let [month, date, year] = new Date().toLocaleDateString('en-US').split('/');
    let [hour, minute, second] = new Date()
      .toLocaleTimeString('en-US')
      .split(/:| /);
     date = new Date(98, 1); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    // Deprecated method; 98 maps to 1998 here as well
    date.setYear(98); // Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

    date.setFullYear(98); // Sat Feb 01 0098 00:00:00 GMT+0000 (BST)

    // Using Date objects
    let start = Date.now();

    // The event to time goes here:
    doSomethingForALongTime();
    let end = Date.now();
    let elapsed = end - start; // elapsed time in milliseconds
    // Using built-in methods
    start = new Date();

    // The event to time goes here:
    doSomethingForALongTime();
    end = new Date();
    elapsed = end.getTime() - start.getTime(); // elapsed time in milliseconds
  }
}


function doSomethingForALongTime() {}

app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}!`);
});
