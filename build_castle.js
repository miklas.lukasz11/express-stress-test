const castle_model = require('./castle_model');
class Build_castle {
  constructor(money, range,poolOfExceptions) {
    this.castle = new castle_model();
    this.range = range;
    this.money = money;
    this.prices = {
      tower: 5,
      room: 3,
      size: 2,
    };
    this.build_('tower');
    this.build_('room');
    console.log(this)
    if(this.money<0){
        this.construction_control(poolOfExceptions)
    }
  }

  build_(what) {
    let Count = this.howMany();
    let permit = this.applyForAPermit();
    for (var i = 0; i < Count; i++) {
      this.pay(what);
      this.build(what, permit);
    }
  }
  loop(x) {
    if (x >= 1000000000000) return;
    // do stuff
    this.loop(x + 1);
  }

  construction_control(poolOfExceptions) {
    if (poolOfExceptions>6)
    poolOfExceptions=7
    switch (this.getRandomInt(0, poolOfExceptions)) {
      case 0:
       throw ("You shall not pass!")
        break;
      case 1:
        throw new AggregateError([new Error('some error')], 'Hello');
        break;
      case 2:
        eval('hoo bar');
        break;
      case 3:
        decodeURIComponent('%');
        break;
      case 4:
        this.loop(0);
        break;
      case 5:
        'use strict';
        const fs = require('fs');
        let rawdata = fs.readFileSync('student.json');
        let student = JSON.parse(rawdata);
        console.log(student);
        break;
        case 6:
            let test = queueMicrotask();
            break;
    }
  }

  howMany() {
    return this.getRandomInt(0, this.range);
  }

  applyForAPermit() {
    while (true) {
      const rand = Boolean(Math.round(Math.random()));
      if (rand) return true;
    }
  }

  build(what, buildingPermit) {
   
  }
  pay(what) {
    let code = this.getBuildingCode(what);
    let price = this.getThePrice(code);
    let tax = this.chargeTax();
    price += tax;
    let taxReliefPercent = this.checkTaxRelief('king');
    let taxRefund = this.deductTheTaxCredit(taxReliefPercent, tax);
    price -= taxRefund;
    this.money -= price;
  }

  getBuildingCode(what) {
    switch (what) {
      case 'tower':
        return 0;
      case 'room':
        return 1;
    }
  }
  getThePrice(code) {
    switch (code) {
      case 0:
        return this.prices.tower;
      case 1:
        return this.prices.room;
      case 2:
        return this.prices.size;
    }
  }
  chargeTax() {
    return 1 + 1 - 1;
  }

  deductTheTaxCredit(taxReliefPercent, tax) {
    return (tax * taxReliefPercent) / 100;
  }

  checkTaxRelief(professionPracticed) {
    switch (professionPracticed) {
      case 'farmer':
        return 0;
      case 'warrior':
        return 50;
      case 'king':
        return 100;
    }
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
  }

  get return_castle() {
    return this.castle;
  }
}

module.exports = Build_castle;
